pipe_str= "videomixer name=m sink_0::xpos=0 sink_0::ypos=0 sink_1::xpos=320 sink_1::ypos=0 ! videoconvert ! \
timeoverlay ! x264enc tune=zerolatency key-int-max=30 bitrate=5000 ! h264parse ! \
capsfilter caps=video/x-h264,profile=constrained-baseline ! h264parse ! rtph264pay ! \
capsfilter caps='"'application/x-rtp,media=video,encoding-name=H264,payload=96'"' ! \
udpsink host=j8.argigs.com port=18004 audiomixer name=audiomix ! audioconvert ! audioresample ! queue ! opusenc ! \
rtpopuspay ! \
udpsink host=j8.argigs.com port=18005 rtpbin name=rtpbin1 udpsrc address=127.0.0.1 port=6000 \
caps='"'application/x-rtp, media=video, encoding-name=VP8, clock-rate=90000'"' ! \
rtpbin1.recv_rtp_sink_1 udpsrc address=127.0.0.1 port=6002 \
caps='"'application/x-rtp, media=audio, encoding-name=OPUS, clock-rate=48000'"' ! \
rtpbin1.recv_rtp_sink_0 rtpbin1. ! rtpvp8depay ! vp8dec ! decodebin ! videoconvert ! queue ! videoscale ! \
video/x-raw, width=320, height=180 ! queue ! m. rtpbin1. ! rtpopusdepay ! opusdec ! audioconvert ! queue ! \
audiomix. rtpbin name=rtpbin2 udpsrc address=127.0.0.1 port=6004 \
caps='"'application/x-rtp, media=video, encoding-name=VP8, clock-rate=90000'"' ! \
rtpbin2.recv_rtp_sink_1 udpsrc address=127.0.0.1 port=6006 \
caps='"'application/x-rtp, media=audio, encoding-name=OPUS, clock-rate=48000'"' ! \
rtpbin2.recv_rtp_sink_0 rtpbin2. ! rtpvp8depay ! vp8dec ! decodebin ! videoconvert ! queue ! \
videoscale ! videobox ! \
 video/x-raw, width=320, height=180 ! queue ! m. rtpbin2. ! rtpopusdepay ! opusdec ! audioconvert ! queue ! audiomix."