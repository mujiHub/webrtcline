import gi

gi.require_version("Gtk", "3.0")
gi.require_version('Notify', '0.7')
from camera2 import Camera2

from gi.repository import Gtk
from gi.repository import Notify

class Handler:
     def onDestroy(self, *args):
         Gtk.main_quit()
     def onRealize(self, _widget):
         print("realize") 
         live_xid=_widget.get_property('window').get_xid()
         cam=Camera2(2, live_xid)


builder = Gtk.Builder()
builder.add_from_file("templates/test.glade")

builder.connect_signals(Handler())
window = builder.get_object("main")
window.set_title("HookCam2222")
  
window.show_all()
Gtk.main()

################################################################

# import gi

# gi.require_version("Gtk", "3.0")
# from gi.repository import Gtk


# class Handler:
#     def onDestroy(self, *args):
#         Gtk.main_quit()

#     def onButtonPressed(self, button):
#         print("Hello World!")


# builder = Gtk.Builder()
# builder.add_from_file("builder_example.glade")
# builder.connect_signals(Handler())

# window = builder.get_object("window1")
# window.show_all()

# Gtk.main()